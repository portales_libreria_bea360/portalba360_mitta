/*!

=========================================================
* Vue Argon Dashboard - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import Vue from 'vue'
import App from './App.vue'
import router from './router/router'
import store from './store'
import './registerServiceWorker'
import ArgonDashboard from './plugins/argon-dashboard'
import Notifications from 'vue-notification'
import apolloProvider from './apollo.graphql'
import VueApollo from 'vue-apollo'
import Element from 'element-ui'
import VueAnalytics from 'vue-analytics';

Vue.use(VueAnalytics, {
  id: 'UA-183240201-2',
  router
});



import '@/plugins/helpers.js'

Vue.config.productionTip = false

Vue.use(ArgonDashboard)
Vue.use(Element)
Vue.use(Notifications)
Vue.use(VueApollo)



new Vue({
  router,
  store,
  provide: apolloProvider.provide(),
  render: h => h(App)
}).$mount('#app')
