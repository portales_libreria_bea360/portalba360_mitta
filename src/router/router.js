import Vue from 'vue'
import Router from 'vue-router'
import DashboardLayout from '@/layout/DashboardLayout'
import store from '@/store'
import AuthLayout from '@/layout/AuthLayout'
Vue.use(Router)

const routes= [
    {
        path: '*',
        name: 'login',
        component: () => import(/* webpackChunkName: "demo" */ '@/views/Usuario/Login.vue')
    },
    {
        path: '/loginAg',
        name: 'loginag',
        component: () => import('@/views/Agente/Login.vue')
    },
    {
        path: '/resultado',
        name: 'resultado',
        component: () => import('@/views/Agente/Resultado.vue')
    },
    {
        path: '/casos/editar/:id',
        name: 'casos.editar',
        component: () => import('@/views/Agente/GuardarCasos.vue')
    },
    {
        path: '/casos/crear',
        name: 'casos.crear',
        component: () => import('@/views/Agente/CrearCasos.vue')
    },
    {
        path: '/contacto',
        name: 'contacto',
        component: () => import('@/views/Agente/ListarContacto.vue')
    },
    {
        path: '/contacto/crear',
        name: 'contacto.crear',
        component: () => import('@/views/Agente/Crearcontacto.vue')
    },
    {
        path: '/contacto/editar/:id',
        name: 'contacto.editar',
        component: () => import('@/views/Agente/GuardarContacto.vue')
    },
    {
        path: '/cuenta',
        name: 'cuenta',
        component: () => import('@/views/Agente/ListarCuenta.vue')
    },
    {
        path: '/cuenta/crear',
        name: 'cuenta.crear',
        component: () => import('@/views/Agente/CrearCuenta.vue')
    },
    {
        path: '/cuenta/editar/:id',
        name: 'cuenta.editar',
        component: () => import('@/views/Agente/GuardarCuentas.vue')
    },
    {
        path: '/notas/:id/:tipo',
        name: 'notas',
        component: () => import('@/views/Agente/Notas.vue')
    },
    {
        path: '/archivos/:id/:tipo',
        name: 'archivos',
        component: () => import('@/views/Agente/Archivos.vue')
    },
    {
        path: '/',
        redirect: 'login',
        component: AuthLayout,
        children: [
            {
                path: '/login',
                name: 'login',
                component: () => import(/* webpackChunkName: "demo" */ '@/views/Usuario/Login.vue')
            },
            {
                path: '/register',
                name: 'register',
                component: () => import(/* webpackChunkName: "demo" */ '@/views/Usuario/Register.vue')
            },
            {
                path: '/recuperarpass',
                name: 'recuperarpass',
                component: () => import(/* webpackChunkName: "demo" */ '@/views/Usuario/Recuperarpass.vue')
            }
        ]
    },
    {
    path: '/',
    redirect: 'mis casos',
    component: DashboardLayout,
    meta: {
        autenticate: true
    },
    children: [
        // {
        //     path: '/dashboard',
        //     name: 'dashboard',
        //     component: () => import(/* webpackChunkName: "demo" */ '@/views/Dashboard.vue')
        // },
        // {
        //     path: '/icons',
        //     name: 'icons',
        //     component: () => import(/* webpackChunkName: "demo" */ '@/views/Icons.vue')
        // },
        {
            path: '/perfil',
            name: 'perfil',
            component: () => import(/* webpackChunkName: "demo" */ '@/views/Usuario/UserProfile.vue')
        },
        // {
        //     path: '/maps',
        //     name: 'maps',
        //     component: () => import(/* webpackChunkName: "demo" */ '@/views/Maps.vue')
        // },
        // {
        //     path: '/tables',
        //     name: 'tables',
        //     component: () => import(/* webpackChunkName: "demo" */ '@/views/Tables.vue')
        // },
        {
            path: '/miscasos',
            name: 'mis casos',
            component: () => import('@/views/Casos/Casos.vue')
        },
        {
            path: 'crear',
            name: 'crear',
            component: () => import('@/views/Casos/Crear.vue')
        },
        {
            path: 'detallecaso/:id',
            name: 'detalle caso',
            component: () => import('@/views/Casos/Detalle.vue')
        },
        {
            path: '/miscuentas',
            name: 'mis cuentas',
            component: () => import('@/views/Cuentas/Cuentas.vue')
        }
    ]
}]

const router = new Router({
    //linkExactActiveClass: 'active',
    mode: 'history',
    routes
})


/**
 * Condición vue, para no poder ingresar por URL a las vistas a menos que este logueado
 */

router.beforeEach((to, from, next) => {
    const status = store.getters['auth/status']
    const homePage = store.getters['auth/homePage']

    let autorizacion = to.matched.some(record => record.meta.autenticate);
    if(!autorizacion && status){
      next(from.name ? false : homePage)
    }else if(autorizacion && !status){
      next('login');
    }else{
      next();
    }
})


export default router