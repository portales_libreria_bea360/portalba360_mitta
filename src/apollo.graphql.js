import { ApolloClient, InMemoryCache, ApolloLink, HttpLink  } from 'apollo-boost'
import VueApollo from 'vue-apollo'
import { Util } from 'portalba360'
import store from './store'

const defaultClient = new ApolloClient({
  link: new ApolloLink((operation, forward) => {
    const instance = new Util()
    // no puedo validar el user, por que el servicio de login no me devuleve nada para guardar en localstorage
    const token = instance.encode({
        token: store.state.validatetoken.instance.token || '',
        url: store.state.validatetoken.instance.url || '',
        company: store.state.validatetoken.instance.company || '',
        usuario: store.state.validatetoken.instance.usuario ?  store.state.validatetoken.instance.usuario.user : ''
    })

    console.log(token)

    operation.setContext({
      headers: {
        authorization: token ? `Bearer ${token}` : ""
      }
    });
    return forward(operation);
  }).concat(
    new HttpLink({
      uri: 'http://localhost:4001/graphql', // Server URL
    })
  ),
  cache: new InMemoryCache()
});

const apolloProvider = new VueApollo({defaultClient})

export default apolloProvider
