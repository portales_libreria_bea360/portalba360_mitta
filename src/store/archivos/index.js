import { Archivo } from 'portalba360'

export default {
    namespaced: true,

    state: ()=>({
        one: {},
        all: [],
        archivo: new Archivo(),
        archivosUp: []
    }),
    getters:{
        one: state => state.one,
        all: state => state.all,
        archivosUp: state => state.archivosUp
    },
    mutations:{
        setOne: (state, data)=>{
            state.one = data
        },
        setAll: (state, data)=>{
            if(data != undefined || data != null){
                state.all = data
            }
        },
        setArchivosUp(state, value) {
            state.archivosUp = value
        }
    },
    actions:{
        async getAll({commit, state},data){
            commit('setLoading', 1, { root: true })
            try{
                let response = await state.archivo.all(data)
                commit('setLoading', -1, { root: true })
                commit('setAll', response.data)
            }catch(e){
                console.log(e)
            }
        },
        async upload({commit, state}, data){
            commit('setLoading', 1, { root: true })
            try{
                let response = await state.archivo.add(data)
                commit('setLoading', -1, { root: true })
                commit('setOne', response)
                return response
            }catch(e){
                return console.log(e)
            }
        },
        async download({commit, state}, data){
            commit('setLoading', 1, { root: true })
            try{
                let response = await state.archivo.download(data)
                commit('setLoading', -1, { root: true })
                commit('setOne', response)
                return response
            }catch(e){
                return console.log(e)
            }
        },
        archivosUp({ commit }, id) {
            commit('setArchivosUp', id)
        },
        async destroyFile({commit, state}, data){
            commit('setLoading', 1, { root: true })
            try{
                let response = await state.archivo.destroy(data)
                commit('setLoading', -1, { root: true })
                commit('setOne', response)
                return response
            }catch(e){
                return console.log(e)
            }
        }
    }
}