import { Agente_auth,
        Agente_caso,
        Agente_tipificacion,
        Agente_contacto,
        Agente_user,
        Agente_prioridad,
        Agente_estado,
        Agente_cuenta,
        Agente_proceso,
        Agente_cf,
        Agente_sla,
        Agente_grupo,
        Agente_nota,
        Agente_archivo,
        Agente_check,
        Agente_parametro
     } from 'portalba360'

export default {
    namespaced: true,

    state: ()=>({
        one: {},
        oneCaso: {},
        oneContacto: {},
        oneCuenta: {},
        instance: JSON.parse(localStorage.getItem('USERINSTANCE')) || {},
        userlogin: new Agente_auth(),
        userCaso: new Agente_caso(),
        userAgente: new Agente_user(),
        userContacto: new Agente_contacto(),
        userTipificacion: new Agente_tipificacion(),
        userPrioridad: new Agente_prioridad(),
        userEstado: new Agente_estado(),
        userCuenta: new Agente_cuenta(),
        userProceso: new Agente_proceso(),
        userCf: new Agente_cf(),
        userSla: new Agente_sla(),
        userGrupo: new Agente_grupo(),
        userNota: new Agente_nota(),
        userArchivo: new Agente_archivo(),
        userCheck: new Agente_check(),
        userParametro: new Agente_parametro(),
        all: [],
        allContacto: [],
        allProductos: [],
        allMotivos: [],
        allSubMotivos: [],
        allPrioridades: [],
        allEstados: [],
        allCuentas: [],
        allCuentaContacto: [],
        allContactoCuenta: [],
        allProcesos: [],
        allCfs: [], 
        allAcuerdos: [],
        allGrupos: [],
        allNota: [],
        allArchivo: [],
        allCheck: [],
        allStages: [],
        userDisponibles: [],
        contactsOfCuenta: {},
        parametrosSystem: [],
        parametrosCustom: []

        
        
    }),
    getters:{
        one: state => state.one,
        all: state => state.all,
        instance: state => state.instance,
        oneCaso: state => state.oneCaso,
        oneContacto: state => state.oneContacto,
        oneCuenta: state => state.oneCuenta,
        allContacto: state => state.allContacto,
        allProductos: state => state.allProductos,
        allMotivos: state => state.allMotivos,
        allSubMotivos: state => state.allSubMotivos,
        allPrioridades: state => state.allPrioridades,
        allEstados: state => state.allEstados,
        allCuentas: state => state.allCuentas,
        allCuentaContacto: state => state.allCuentaContacto,
        allContactoCuenta: state => state.allContactoCuenta,
        allProcesos: state => state.allProcesos,
        allCfs: state => state.allCfs,
        allAcuerdos: state => state.allAcuerdos,
        allGrupos: state => state.allGrupos,
        allNota: state => state.allNota,
        allArchivo: state => state.allArchivo,
        userLogin: state => state.userLogin,
        allCheck: state => state.allCheck,
        allStages: state => state.allStages,
        contactsOfCuenta: state => state.contactsOfCuenta,
        userDisponibles: state => state.userDisponibles,
        parametrosSystem: state => state.parametrosSystem,
        parametrosCustom: state => state.parametrosCustom
    },
    mutations:{
        setOne: (state, data)=>{
            state.one = data
        },
        setAll: (state, data)=>{
            if(data != undefined || data != null){
                state.all = data
            }
        },
        setInstance(state, data){
            state.instance = data
            localStorage.setItem('USERINSTANCE', JSON.stringify(data))
        },
        setOneCaso(state, data){
            state.oneCaso = data
        },
        setAllContacto(state, data){
            state.allContacto = data
        },
        setAllProductos(state, data){
            state.allProductos = data
        },
        setAllMotivos(state, data){
            state.allMotivos = data
        },
        setAllSubMotivos(state, data){
            state.allSubMotivos = data
        },
        setAllPrioridad(state, data){
            state.allPrioridades = data
        },
        setAllEstado(state, data){
            state.allEstados = data
        },
        setAllCuenta(state, data){
            state.allCuentas = data
        },
        setAllCuentaContacto(state, data){
            state.allCuentaContacto = data
        },
        setAllContactoCuenta(state, data){
            state.allContactoCuenta = data
        },
        setAllProceso(state, data){
            state.allProcesos = data
        },
        setAllCf(state, data){
            state.allCfs = data
        },
        setOneContacto(state, data){
            state.oneContacto = data
        },
        setAllAcuerdo(state, data){
            state.allAcuerdos = data
        },
        setAllGrupo(state, data){
            state.allGrupos = data
        },
        setOneCuenta(state, data){
            state.oneCuenta = data
        },
        setAllNota(state, data){
            state.allNota = data
        },
        setAllArchivo(state, data){
            state.allArchivo = data
        },
        setAllCheck(state, data){
            state.allCheck= data
        },
        setAllStages(state, data){
            state.allStages= data
        },
        setContactsOfCuenta(state, data){
            state.contactsOfCuenta= data
        },
        setUserDisponibles(state, data){
            state.userDisponibles = data
        },
        setParametrosSystem(state, data){
            state.parametrosSystem = data
        },
        setParametrosCustom(state, data){
            state.parametrosCustom = data
        }
    },
    actions:{
        //login metodos
        async validateToken({commit, state},data){
            commit('setLoading', 1, { root: true })
            try {
                let response = await state.userlogin.loginToken(data)
                commit('setLoading', -1, { root: true })
                return response
            } catch (error){
                commit('setLoading', -1, { root: true })
                return Promise.reject(error.response.data)
            }
        },
        async login({commit, state},data){
            commit('setLoading', 1, { root: true })
            try {
                let response = await state.userlogin.login(data)
                console.log(response);
                commit('setLoading', -1, { root: true })
                if (response.type){
                    commit('setInstance', response)
                }
                return response
            } catch (error){
                commit('setLoading', -1, { root: true })
                console.log(error);
                return Promise.reject(error)
            }
        },
        async authToken({commit, state},data){
            commit('setLoading', 1, { root: true })
            try {
                let response = await state.userlogin.loginSE(data)
                commit('setLoading', -1, { root: true })
                if (response.type){
                    commit('setInstance', response)
                    return response
                }
            } catch (error){
                commit('setLoading', -1, { root: true })
                return Promise.reject(error.response.data)
            }
        },
        //casos metodos
        async getCasos({commit, state},data){
            commit('setLoading', 1, { root: true })
            try {
                let response = await state.userCaso.all(data)
                console.log(response)
                commit('setLoading', -1, { root: true })
                commit('setAll', response.data)
                return response
            } catch (error){
                console.log(error);
                return Promise.reject(error.response.data)
            }
        },
        async getAsignadoGrupo({commit, state},data){
            commit('setLoading', 1, { root: true })
            try {
                let response = await state.userCaso.asignadoagrupo(data)
                commit('setLoading', -1, { root: true })
                commit('setAll', response)
                return response
            } catch (error){
                return Promise.reject(error.response.data)
            }
        },

        async getBy({commit, state},data){
            try {
                let response = await state.userCaso.one(data)
                commit('setLoading', -1, { root: true })
                if (data.method != undefined) {
                    commit('setOneCaso', response.data)
                    commit('setAllCf', response.cfs)
                    commit('setAllPrioridad', response.prioridades)
                    commit('setAllEstado', response.estados)
                    commit('setAllCheck', response.checklist)
                    commit('setAllContacto', response.contactos)
                    commit('setAllProductos', response.productos)
                    commit('setAllMotivos', response.motivos)
                    commit('setAllSubMotivos', response.submotivos)
                    commit('setAllCuenta', response.cuentas)
                    commit('setAllProceso', response.procesos)
                    commit('setAllStages', response.pasos)
                    commit('setUserDisponibles', response.usuarios)
                } else {
                    commit('setOneCaso', response)
                }
                return response
            } catch (error){
                return Promise.reject(error)
            }
        },

        async save({commit, state},data){
            try {
                let response = await state.userCaso.upd(state.oneCaso)
                commit('setLoading', -1, { root: true })
                commit('setOneCaso', response)
                return response
            } catch (error){
                return Promise.reject(error.response.data)
            }
        },
        async add({commit, state},data){
            try {
                let response = await state.userCaso.add(data)
                commit('setLoading', -1, { root: true })
                commit('setOneCaso', response)
                return response
            } catch (error){
                return Promise.reject(error.response.data)
            }
        },

        async usuarios({commit, state},data){
            try {
                let response = await state.userAgente.getdisponibles(data)
                commit('setLoading', -1, { root: true })
                commit('setUserDisponibles', response)
                return response
            } catch (error){
                return Promise.reject(error.response.data)
            }
        },

        async getAllNotas({commit, state},data){
            try {
                let response = await state.userNota.all(data)
                console.log(response);
                commit('setLoading', -1, { root: true })
                commit('setAllNota', response.data)
                return response
            } catch (error){
                return Promise.reject(error.response.data)
            }
        },
        async addNota({commit, state},data){
            try {
                let response = await state.userNota.add(data)
                commit('setLoading', -1, { root: true })
                return response
            } catch (error){
                return Promise.reject(error.response.data)
            }
        },

        async getAllArchivos({commit, state},data){
            try {
                let response = await state.userArchivo.all(data)
                commit('setLoading', -1, { root: true })
                commit('setAllArchivo', response)
                return response
            } catch (error){
                return Promise.reject(error.response.data)
            }
        },

        async addArchivo({commit, state},data){
            try {
                let response = await state.userArchivo.add(data)
                commit('setLoading', -1, { root: true })
                commit('setAllArchivo', response)
                return response
            } catch (error){
                return Promise.reject(error.response.data)
            }
        },
        
        
        async download({commit, state},data){
            try {
                let response = await state.userArchivo.download(data)
                commit('setLoading', -1, { root: true })
                commit('setAllArchivo', response)
                return response
            } catch (error){
                return Promise.reject(error.response.data)
            }
        },
        async destroyFile({commit, state},data){
            try {
                let response = await state.userArchivo.destroy(data)
                commit('setLoading', -1, { root: true })
                commit('setAllArchivo', response)
                return response
            } catch (error){
                return Promise.reject(error.response.data)
            }
        },
        
        
        //metodos de contacto
        async getContacto({commit, state},data){
            try{
                let response = await state.userContacto.all(data)
                console.log(response)
                commit('setLoading', -1, { root: true })
                commit('setAllContacto', response.data)
                return response
            } catch (error){
                console.log(error);
                return Promise.reject(error.response.data)
            }
        },
        async getByContacto({commit, state},data){
            try {
                let response = await state.userContacto.one(data)
                commit('setLoading', -1, { root: true })
                if (data.method != undefined) {
                    commit('setOneContacto', response.data)
                    commit('setAllAcuerdo', response.acuerdos)
                    commit('setAllCf', response.cfs)
                    commit('setAllGrupo', response.grupos)
                    return response
                } else {
                    commit('setOneContacto', response)
                    return response
                }
            } catch (error){
                return Promise.reject(error.response.data)
            }
        },
        async addContacto({commit, state},data){
            try {
                let response = await state.userContacto.add(data)
                commit('setLoading', -1, { root: true })
                commit('setOneContacto', response)
                return response
            } catch (error){
                return Promise.reject(error.response.data)
            }
        },
        async saveContacto({commit, state},data){
            try {
                let response = await state.userContacto.upd(data)
                commit('setLoading', -1, { root: true })
                commit('setOneContacto', response)
                return response
            } catch (error){
                return Promise.reject(error.response.data)
            }
        },
        

        async getByGruposSkill({commit, state},data){
            try {
                let response = await state.userGrupo.all(data)
                commit('setLoading', -1, { root: true })
                commit('setAllGrupo', response)
                return response
            } catch (error){
                return Promise.reject(error.response.data)
            }
        },

        async getAllAcuerdo({commit, state},data){
            try {
                let response = await state.userSla.all(data)
                commit('setLoading', -1, { root: true })
                commit('setAllAcuerdo', response)
                return response
            } catch (error){
                return Promise.reject(error.response.data)
            }
        },

        async getAllProductos({commit, state},data){
            try {
                let response = await state.userTipificacion.productosAll(data)
                commit('setLoading', -1, { root: true })
                commit('setAllProductos', response)
                return response
            } catch (error){
                return Promise.reject(error.response.data)
            }
        },

        async getAllMotivos({commit, state},data){
            try {
                let response = await state.userTipificacion.motivosAll(data)
                commit('setLoading', -1, { root: true })
                commit('setAllMotivos', response)
                return response
            } catch (error){
                return Promise.reject(error.response.data)
            }
        },

        async getAllSubMotivos({commit, state},data){
            try {
                let response = await state.userTipificacion.subMotivosAll(data)
                commit('setLoading', -1, { root: true })
                commit('setAllSubMotivos', response)
                return response
            } catch (error){
                return Promise.reject(error.response.data)
            }
        },

        async getPrioridades({commit, state},data){
            try {
                let response = await state.userPrioridad.all(data)
                commit('setLoading', -1, { root: true })
                commit('setAllPrioridad', response)
                return response
            } catch (error){
                return Promise.reject(error.response.data)
            }
        },

        async getAllEstados({commit, state},data){
            try {
                let response = await state.userEstado.all(data)
                commit('setLoading', -1, { root: true })
                commit('setAllEstado', response)
                return response
            } catch (error){
                return Promise.reject(error.response.data)
            }
        },

        async getAllCuentas({commit, state},data){
            try {
                let response = await state.userCuenta.all(data)
                commit('setLoading', -1, { root: true })
                commit('setAllCuenta', response.data)
                return response
            } catch (error){
                return Promise.reject(error.response.data)
            }
        },
        async getByCuenta({commit, state},data){
            try {
                let response = await state.userCuenta.one(data)
                commit('setLoading', -1, { root: true })
                if (data.method != undefined) {
                    commit('setOneCuenta', response.data)
                    commit('setAllAcuerdo', response.acuerdos)
                    commit('setAllCf', response.cfs)
                    return response
                } else {
                    commit('setOneCuenta', response)
                    return response
                }
            } catch (error){
                return Promise.reject(error.response.data)
            }
        },
        async addCuenta({commit, state},data){
            try {
                let response = await state.userCuenta.add(data)
                commit('setLoading', -1, { root: true })
                commit('setOneCuenta', response)
                return response
            } catch (error){
                return Promise.reject(error.response.data)
            }
        },

        async saveCuenta({commit, state},data){
            let final = {}
            if (Object.keys(state.oneCuenta.cf).length !== 0) {
                final = {
                    ...state.oneCuenta,
                    ...data
                }
            }
            try {
                let response = await state.userCuenta.upd(final)
                commit('setLoading', -1, { root: true })
                commit('setOneCuenta', response)
                return response
            }catch (error){
                return Promise.reject(error.response.data)
            }
        },
        async getContacts({commit, state},data){
  
            try {
                let response = await state.userCuenta.getcontactsbyid(data)
                commit('setLoading', -1, { root: true })
                commit('setContactsOfCuenta', response)
                return response
            }catch (error){
                return Promise.reject(error.response.data)
            }
        },
        
        async getAllProcesos({commit, state},data){
            try {
                let response = await state.userProceso.all(data)
                commit('setLoading', -1, { root: true })
                commit('setAllProceso', response.data)
                return response
            } catch (error){
                return Promise.reject(error.response.data)
            }
        },
        async nextCase({commit, state},data){
            try {
                let response = await state.userProceso.next(data)
                commit('setLoading', -1, { root: true })
                //commit('setAllProceso', response)
                return response
            } catch (error){
                return Promise.reject(error.response.data)
            }
        },
        async backCase({commit, state},data){
            try {
                let response = await state.userProceso.back(data)
                commit('setLoading', -1, { root: true })
                //commit('setAllProceso', response)
                return response
            } catch (error){
                return Promise.reject(error.response.data)
            }
        },

        async getStages({commit, state},data){
            try {
                let response = await state.userProceso.stages(data)
                commit('setLoading', -1, { root: true })
                commit('setAllStages', response)
                return response
            } catch (error){
                return Promise.reject(error.response.data)
            }
        },
        async getAllCheck({commit, state},data){
            try {
                let response = await state.userCheck.all(data)
                commit('setLoading', -1, { root: true })
                commit('setAllCheck', response)
                return response
            } catch (error){
                return Promise.reject(error.response.data)
            }
        },
        async getAllCfs({commit, state},data){
            try {
                let response = await state.userCf.all(data)
                commit('setLoading', -1, { root: true })
                commit('setAllCf', response)
                return response
            } catch (error){
                return Promise.reject(error.response.data)
            }
        },
        async getParametrosSystem({ commit, state }, data) {
            try {
                let response = await state.userParametro.systemAll(data)
                commit('setLoading', -1, { root: true })
                commit('setParametrosSystem', response)
                return response
            } catch (e) {
                return Promise.reject(e)
            }
        },
        async getParametrosCustom({ commit, state }, data) {
            try {
                let response = await state.userParametro.customAll(data)
                commit('setLoading', -1, { root: true })
                commit('setParametrosCustom', response)
                return response
            } catch (e) {
                return Promise.reject(e)
            }
        },
        async check({ commit, state }, data) {
            try {
                let response = await state.userCheck.check(data)
                commit('setLoading', -1, { root: true })
                return response
            } catch (e) {
                return Promise.reject(e)
            }
        },
        async scriptEjecute({commit, state},data){
            try {
                let response = await state.userCheck.executescript(data)
                commit('setLoading', -1, { root: true })
                return response
            } catch (error){
                return Promise.reject(error.response.data)
            }
        },
    }
}