import { Cuenta } from 'portalba360'

export default {
    namespaced: true,

    state: ()=>({
        one: {},
        all: [],
        cuenta: new Cuenta()
    }),
    getters:{
        one: state => state.one,
        all: state => state.all
    },
    mutations:{
        setOne: (state, data)=>{
            state.one = data
        },
        setAll: (state, data)=>{
            if(data != undefined || data != null){
                state.all = data
            }
        }
    },
    actions:{
        async getAll({commit, state},data){
            commit('setLoading', 1, { root: true })
            try{
                let response = await state.cuenta.all(data)
                commit('setLoading', -1, { root: true })
                commit('setAll', response.data)
            }catch(e){
                console.log(e)
            }
        }
    }
}