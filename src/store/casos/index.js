import { Caso, Custom } from 'portalba360'

export default {
    namespaced: true,

    state: ()=>({
        one: {},
        all: [],
        paginationCasos: {},
        indicadores: {},
        Allproductos: [],
        Allmotivos: [],
        Allsubmotivos: [],
        producto: '',
        motivo: '',
        subMotivo: '',
        caso: new Caso(),
        custom: new Custom()

    }),
    getters:{
        one: state => state.one,
        all: state => state.all,
        indicadores: state => state.indicadores,
        Allproductos: state => state.Allproductos,
        Allmotivos: state => state.Allmotivos,
        Allsubmotivos: state => state.Allsubmotivos,
        producto: state => state.producto,
        motivo: state => state.motivo,
        subMotivo: state => state.subMotivo,
        paginationCasos: state => state.paginationCasos
    },
    mutations:{
        setOne: (state, data)=>{
            state.one = data
        },
        setAll: (state, data)=>{
            if(data != undefined || data != null){
                state.all = data
            }
        },
        setIndicadores: (state, data)=>{
            if (data) {     
                state.indicadores = Object.assign(data)
            }
        },
        setProductos: (state, data)=>{
            if(data){
                state.Allproductos = data
            }
        },
        setMotivos: (state, data)=>{
            if(data){
                state.Allmotivos = data
            }
        },
        setSubMotivos: (state, data)=>{
            if(data){
                state.Allsubmotivos = data
            }
        },
        setProducto: (state, data)=>{
            if(data || data === ''){
                state.producto = data
            }
        },
        setMotivo: (state, data)=>{
            if(data || data === ''){
                state.motivo = data
            }
        },
        setSubMotivo: (state, data)=>{
            if(data || data === ''){
                state.subMotivo = data
            }
        },
        setPaginationCasos: (state, data)=>{
            state.paginationCasos = data
        }
    },
    actions:{
        async getAll({commit, state}, data){
            commit('setLoading', 1, { root: true })
            try{
                let response = await state.caso.all(data)
                commit('setLoading', -1, { root: true })
                commit('setAll', response.data.data)
                commit('setPaginationCasos', response.data.pagination)
            }catch(e){
                return console.log(e)
            }
        },
        async getOne({commit, state}, data){
            commit('setLoading', 1, { root: true })
            try{
                let response = await state.caso.one(data)
                commit('setLoading', -1, { root: true })
                commit('setOne', response.data[0])
            }catch(e){
                return console.log(e)
            }
        },
        async createCaso({commit, state}, data){
            commit('setLoading', 1, { root: true })
            try{
                let response = await state.caso.add(data)
                commit('setLoading', -1, { root: true })
                commit('setOne', response.data)
                return response.data
            }catch(e){
                return console.log(e)
            }
        },
        async getIndicadores({commit, state}, data){
            commit('setLoading', 1, { root: true })
            try{
                let response = await state.caso.indicadores(data)
                commit('setLoading', -1, { root: true })
                commit('setIndicadores', response)
            }catch(e){
                return console.log(e)
            }
        },
        async getProductos({commit, state}, data){
            commit('setLoading', 1, { root: true })
            try{
                let response = await state.caso.productos(data)
                commit('setLoading', -1, { root: true })
                commit('setProductos', response.data)
            }catch(e){
                return console.log(e)
            }
        },
        async getMotivos({commit, state}, data){
            commit('setLoading', 1, { root: true })
            try{
                let response = await state.caso.motivos(data)
                commit('setLoading', -1, { root: true })
                commit('setMotivos', response.data)
            }catch(e){
                return console.log(e)
            }
        },
        async getSubMotivos({commit, state}, data){
            commit('setLoading', 1, { root: true })
            try{
                let response = await state.caso.subMotivos(data)
                commit('setLoading', -1, { root: true })
                commit('setSubMotivos', response.data)
            }catch(e){
                return console.log(e)
            }
        },
        async custom({commit, state}, data){
            commit('setLoading', 1, { root: true })
            try{
                let response = await state.custom.service(data)
                commit('setLoading', -1, { root: true })
                commit('setOne', response.data)
                return response.data
            }catch(e){
                return console.log(e)
            }
        }
    }
}