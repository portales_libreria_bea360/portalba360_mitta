import Vue from 'vue'
import Vuex from 'vuex'

import auth from './auth'
import casos from './casos'
import notas from './notas'
import cuentas from './cuentas'
import archivos from './archivos'
import parametros from './parametros'
import validatetoken from './validatetoken'


Vue.use(Vuex)

export default new Vuex.Store({
    state: () => ({
        loading: 0
    }),
    getters:{
        loading: state => state.loading > 0
    },
    mutations:{
        setLoading(state, value) {
            state.loading += value
        }
    },
    actions:{
    
    },
    modules:{
        auth,
        casos,
        notas,
        cuentas,
        archivos,
        parametros,
        validatetoken
    }
})