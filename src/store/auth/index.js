import { Auth } from 'portalba360'
import isEmpty from 'lodash/isEmpty'

export default {
    namespaced: true,
    
    state: () => ({
        status: !!JSON.parse(localStorage.getItem('status')),
        recordar: !!JSON.parse(localStorage.getItem('recordar')) || '',
        auth: new Auth(),
        one: {},
        contacto: {}
    }),
      getters: {
            status: state => state.status,
            recordar: state => state.recordar,
            contacto: state => state.contacto,
            userAvatarName: state => (state.contacto.email[0] && state.contacto.nombre && state.contacto.apellido ? state.contacto.nombre[0] + state.contacto.apellido[0] : ''),
            homePage: () => {    
                return { name: 'mis casos'}
            },
            one: state => state.one
      },
    
    mutations: {
     
        /**
         * funciones que asignan valores de un usuario valido,
         * las variables quedan guardades en sessionStorage localstorage.
         */
        setStatus(state, value) {
            if(typeof value === 'boolean'){
                state.status = value
                localStorage.setItem('status', value)
            }else{
                localStorage.removeItem('status')
            }
        },
        setRecordar(state, value){
            if(typeof value === 'boolean'){
                state.recordar = value
                localStorage.setItem('recordar', value)
            }else{
                localStorage.removeItem('recordar')
            }
        },
        setOne: (state, data)=>{
            state.one = data
        },
        setContacto: (state, data)=>{
            if (data){
                state.contacto = data
            } else {
                if (!state.recordar && !state.status){
                    state.contacto = {}
                } else {
                    state.contacto = state.auth.one('contacto')
                }
            }
        }
    },
    actions: {
        /**
         * Función que es llamada desde el componente Login, para posteriromente ejeuctar la funcion de login
         * que se encuentra en la libreria, si todo sale bien crear variables globales con los datos del usuario.
         */
        async login({commit, state, dispatch}, objeto){
            commit('setLoading', 1, { root: true })
            let data = {
                "company": objeto.company,
                "email": objeto.email,
                "pass": objeto.pass,
                "url": objeto.url
            }

            try{
                const response = await state.auth.login(data)
                if (response.data.contacto){
                    if (objeto.checked){
                        commit('setRecordar', true);
                    }else {
                        commit('setRecordar', false);
                    }
                    commit('setContacto', state.auth.one('contacto'))
                    commit('setLoading', -1, { root: true })
                    commit('setStatus', true);
                    return response;
                }
            }catch (e) {
                commit('setLoading', -1, { root: true })
                dispatch('logout')
                return Promise.reject(e.response.data)
            }
        },
        logout({ state, commit }) {
            commit('setStatus', false)
            state.auth.logout(state.recordar)
            return Promise.resolve()
        },
        limpiar({ commit }) {
            commit('setRecordar', false)
            return Promise.resolve()
        },
        async create({commit, state}, data){
            commit('setLoading', 1, { root: true })
            try{
                let response = await state.auth.add(data)
                commit('setLoading', -1, { root: true })
                commit('setOne', response)
                return response
            }catch(e){
                commit('setLoading', -1, { root: true })
                return Promise.reject(e.response.data)
            }
        },
        async recuperarPass({commit, state}, data){
            commit('setLoading', 1, { root: true })
            try{
                let response = await state.auth.recovery(data)
                commit('setLoading', -1, { root: true })
                commit('setOne', response)
                return response
            }catch(e){
                commit('setLoading', -1, { root: true })
                return Promise.reject(e.response.data)
            }
        },
        async save({commit, state, dispatch}, data){
       
            commit('setLoading', 1, { root: true })
            try{
                commit('setLoading', -1, { root: true })
                const response = await state.auth.upd(data)
                commit('setOne', response.data)
                return response.data
            }catch (e) {
                commit('setLoading', -1, { root: true })
                dispatch('logout')
                return Promise.reject(new Error())
            }
        },
        async updPass({commit, state, dispatch}, data){
            commit('setLoading', 1, { root: true })
            try{
                commit('setLoading', -1, { root: true })
                const response = await state.auth.updPass(data)
                return response
            }catch (e) {
                commit('setLoading', -1, { root: true })
                return Promise.reject(e.response.data)
            }
        },
        async getBy({commit, state}, data){
                commit('setLoading', 1, { root: true })
            try{
                let response = await state.auth.getBy(data)
                commit('setLoading', -1, { root: true })
                commit('setOne', response.data[0])
            }catch(e){
                commit('setLoading', -1, { root: true })
                return Promise.reject(e.response.data)
            }
        }
    }
}
