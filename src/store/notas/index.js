import { Nota, Custom } from 'portalba360'

export default {
    namespaced: true,

    state: ()=>({
        one: {},
        all: [],
        nota: new Nota(),
        custom: new Custom()
    }),
    getters:{
        one: state => state.one,
        all: state => state.all
    },
    mutations:{
        setOne: (state, data)=>{
            state.one = data
        },
        setAll: (state, data)=>{
            if(data != undefined || data != null){
                state.all = data
            }
        }
    },
    actions:{
        async getAll({commit, state},casos){
            commit('setLoading', 1, { root: true })
            try{
                let response = await state.nota.all(casos)
                commit('setLoading', -1, { root: true })
                commit('setAll', response.data)
            }catch(e){
                console.log(e)
            }
        },
        async save({commit, state}, data){
            commit('setLoading', 1, { root: true })
            try{
                let response = await state.nota.add(data)
                commit('setLoading', -1, { root: true })
                commit('setOne', response.data)
                return response.data
            }catch(e){
                return console.log(e)
            }
        },
        async custom({commit, state}, data){
            commit('setLoading', 1, { root: true })
            try{
                let response = await state.custom.service(data)
                commit('setLoading', -1, { root: true })
                commit('setOne', response.data)
                return response.data
            }catch(e){
                return console.log(e)
            }
        }
    }
}