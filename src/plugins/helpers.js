import Vue from 'vue'

const plugin = {
	install(Vue, options) {
		Vue.prototype.$notification = function(type, msg) {
			this.$notify({
                group: 'foo',
                title: 'Mensaje',
                type: type,
                speed: 1000,
                duration: 500,
                text: msg || (type === 'success' ? 'Los cambios se han guardado correctamente.' : 'Ocurrió un error al intentar realizar la operación.')
            });
		}
		Vue.prototype.$resetData = function() {
            Object.assign(this.$data, this.$options.data.apply(this))
        }
        // Vue.prototype.$resetForm = function(form) {
        //     if (this.$refs[form]) {
        //         this.$refs[form].clearValidate()
        //     }
        // }
        // Vue.prototype.$validateForm = async function(form) {
        //     try {
        //         let valid = await this.$refs[form].validate().catch(() => {
        //             this.$message({ type: 'error', showClose: true, message: 'Ups, existen campos con errores.' })
        //             return false
        //         })

        //         return Promise.resolve(valid)
        //     } catch (e) {
        //         return Promise.resolve(false)
        //     }
        // }
	}
}

Vue.use(plugin)